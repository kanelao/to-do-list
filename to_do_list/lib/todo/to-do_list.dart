import 'package:flutter/material.dart';
import 'package:to_do_list/todo/todo.dart';

typedef ToggleTodoCallback = void Function(Todo, bool);

class TodoList extends StatelessWidget {
  final List<Todo> todos;
  final ToggleTodoCallback onChanged;

  TodoList({@required this.todos, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildItem,
      itemCount: todos.length,
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    final todo = todos[index];
    return CheckboxListTile(
        value: todo.isDone,
        title: Text(todo.title),
        onChanged: (bool isChecked) {
          onChanged(todo, isChecked);
        });
  }
}
