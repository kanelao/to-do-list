import 'package:flutter/material.dart';
import 'todo.dart';

class NewTodoDialog extends StatelessWidget {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('New Todo'),
      content: TextField(
        controller: controller,
        autofocus: false,
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text('Add'),
          onPressed: () {
            Navigator.of(context).pop(Todo(title: controller.value.text));
            controller.clear();
          },
        ),
      ],
    );
  }
}
